FROM registry.gitlab.com/gitlab-org/terraform-images/stable:latest

#ANSIBLE
RUN apk add python3 \
    && apk add ansible

#HELM
ARG HELM_VERSION=3.7.2
ADD https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz helm-v${HELM_VERSION}-linux-amd64.tar.gz 
RUN tar -zxvf helm-v${HELM_VERSION}-linux-amd64.tar.gz \
    && mv linux-amd64/helm /usr/local/bin/helm

#KUBECTL
RUN curl -LO https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl \
    && chmod +x ./kubectl \
    && mv ./kubectl /usr/local/bin/kubectl \
    && kubectl version --client

#TPA
ARG TPA_VERSION=2.5.0
ADD https://github.com/radekg/terraform-provisioner-ansible/releases/download/v${TPA_VERSION}/terraform-provisioner-ansible-linux-amd64_v${TPA_VERSION} /root/.terraform.d/plugins/terraform-provisioner-ansible
RUN chmod 755 /root/.terraform.d/plugins/terraform-provisioner-ansible
